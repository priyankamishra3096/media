from flask import Flask, request, jsonify, send_file
import pymongo
from gridfs import GridFS
from pymongo import MongoClient
from datetime import datetime
import time

app = Flask(__name__)

mongo_client = MongoClient("mongodb://localhost:27017/")
db = mongo_client['test']


def media_upload(fs, name, file_data, db, content_type, media_type, media_id):
    media_obj_id = db.media_data.files.find_one({"media_id": media_id}, {"_id": 1})
    if media_obj_id:
        fs.delete(media_obj_id['_id'])
    upload = fs.put(file_data, filename=name, contentType=content_type, media_id=media_id, media_type=media_type)
    return {"success": True, "msg": "Data Uploaded Successfully"}


def create_id(name):
    dt = datetime.utcnow().strftime("%Y%m%d%H%M%S%f")
    # today = time.strftime("%Y-%m-%d %H:%M:%S.%f")
    media_id = name + dt
    media_id = media_id.replace(" ", "_")
    print(media_id)
    return media_id


"""
API for CREATING AND UPDATING DATA :

FOR CREATE:
__________________ 
media_type:Song
media_name:test media
duration:20
media :  an audio file

FOR UPDATE:
________________________
media_id:test media20210314141314003442
FOR CREATE: 
media_type:Song
media_name:test media
duration:20
media :  an audio file

"""


@app.route("/upload_media", methods=["POST"])
def upload_media():
    # if request.method == "POST":
    media_type = request.form.get('media_type')
    name = request.form.get("media_name")
    media_id = create_id(name)
    # song_name = request.form.get("media_name")
    duration = request.form.get("duration")
    res_data = {}
    if request.files:
        file_item = request.files.getlist("media")
        media_file = file_item[0]
        file_data = media_file.read()
        content_type = media_file.content_type
    if file_data:
        fs = GridFS(db, "media_data")
        if media_type == "Song":
            res_data['media_id'] = media_id
            res_data['song_name'] = name
            res_data["duration"] = duration
            res_data['upload_time'] = datetime.utcnow()
        elif media_type == "Podcast":
            host = request.form.get("host")
            participants = request.form.get("participants")
            res_data['media_id'] = media_id
            res_data['podcast_name'] = name
            res_data["duration"] = duration
            res_data['upload_time'] = datetime.utcnow()
            res_data["host"] = host
            res_data["participants"] = participants
        elif media_type == "Audiobook":
            author = request.form.get("author")
            res_data['media_id'] = media_id
            res_data['title'] = name
            res_data["duration"] = duration
            res_data['upload_time'] = datetime.utcnow()
            res_data['author'] = author
        if media_type in ["Song","Podcast","Audiobook"]:
            result, data = media_upload(fs, name, file_data, db, content_type, media_type, media_id)
            if request.form.get("media_id"):
                qry_check = db.media_info.find_one({"media_id": request.form.get("media_id")})
                if qry_check:
                    qry_update = db.media_info.update_one({"media_id": request.form.get("media_id")}, {"$set": res_data})
                    msg = "Data Updated Successfully."
                    res = True
                else:
                    msg = "Data Not Found."
                    res = True
            else:
                qry = db.media_info.insert_one(res_data)
                msg = "Data Inserted Successfully."
                res = True
        else:
            res = False
            msg = "Please Insert a valid Type"
    return jsonify({"Success": res, "msg": msg})


"""
API for getting stored audio file content.

URL :  http://0.0.0.0:5004/get_media/test media20210314141314003442/Song
"""


@app.route("/get_media/<media_id>/<media_type>", methods=["GET"])
def get_media(media_id, media_type):
    fs = GridFS(db, "media_data")
    # media_name = media_name.strip()
    media_obj_id = db.media_data.files.find_one({"media_id": media_id}, {"_id": 1, "contentType": 1, "filename": 1})
    if media_obj_id:
        try:
            out = fs.get(media_obj_id["_id"])
            return send_file(out, attachment_filename=media_obj_id['filename'], mimetype=media_obj_id["contentType"])
        except Exception as err:
            return jsonify({"Success": False, "msg": "Not Found %s %s" % (media_type, str(err))}), 404
    else:
        return jsonify({"Success": False, "msg": "Not Found"}), 404


"""
API for deleting Audio file data:

URL : http://0.0.0.0:5004/delete_media/test media20210314141314003442/Song
"""


@app.route("/delete_media/<media_id>/<media_type>", methods=["DELETE"])
def delete_media(media_id, media_type):
    fs = GridFS(db, "media_data")
    # media_name = media_name.strip()
    media_obj_id = db.media_data.files.find_one({"media_id": media_id}, {"_id": 1, "filename": 1})
    if media_obj_id:
        try:
            qry = fs.delete(media_obj_id['_id'])
            db.media_info.delete_one({"media_id": media_id})
            return jsonify({"Success": True, "msg": "Data Removed Successfully"}), 200
        except Exception as err:
            return jsonify({"Success": False, "msg": "Not Found %s %s" % (media_type, str(err))}), 404
    else:
        return jsonify({"Success": False, "msg": "Not Found"}), 404


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5004)
